#!/bin/bash

# set env vars for Java app (which will be read in DatabaseConfig.java)
export DB_USER=admin
export DB_USER=admin
export DB_PWD=adminpass
export DB_SERVER=localhost
export DB_NAME=team-projects

export MYSQL_ROOT_PASSWORD=rootpass

export PMA_HOST=mysql
export PMA_PORT=3306


# build JAVA app
docker build -t {server-ip-address}:{port}/java-app:1.0-SNAPSHOT .

# push artifcats to remote docker repository on Nexus
docker push {server-ip-address}:{port}/java-app:1.0-SNAPSHOT
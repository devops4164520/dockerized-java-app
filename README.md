# dockerized-java-app

Features  well-defined Java , MySQL, PHPmyadmin images using Docker compose. It provides a streamlined process of setting up the environment, containerizing the Java application with MySQL database quickly and efficiently deploying it locally or to a remote repository.

The Dockerfile features a multi-stage build process ensuring the production of a lightweight final java-app image.

![diagram](architecture.png)

## Usage
- Ensure env variables, and server-ip address are replaced in `configure.sh`and `docker-compose.yaml` respectively.
- RUN `configure.sh` to build and push the aritifact to a remote repository configured.
- `docker-compose -f docker-compose.yaml up` to start the containers.
- `docker-compose -f docker-compose.yaml down` to tear down the containers.

## Note
- Adapt the configurations and settings to suit your specific environment and use case.
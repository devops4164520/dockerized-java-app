# Multi-stage build

# Base image with Java Installed as build
FROM gradle:jdk17 AS build
WORKDIR /app
COPY build.gradle settings.gradle gradlew gradlew.bat /app/
COPY gradle /app/gradle

# Copy the app source code
COPY src /app/src

# Build the application
RUN gradle build

#RUN ./gradlew clean build

# Lightweight RUNTIME image for the runtime environment
FROM ubuntu/jre:8_edge
EXPOSE 8080
WORKDIR /app
# Copy app JAR file from build stage
COPY --from=build /app/build/libs/dockerized-java-app-1.0-SNAPSHOT.jar /app/
CMD ["java", "-jar", "dockerized-java-app-1.0-SNAPSHOT.jar"]